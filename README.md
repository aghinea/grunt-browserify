# README #

### What is this repository for? ###

* Grunt-Browserify
* 0.0.1


### Setup ###

* install Node.js (https://nodejs.org);
* install Grunt cli (Run "npm install -g grunt-cli");
* install Ruby (http://dl.bintray.com/oneclick/rubyinstaller/rubyinstaller-2.2.2-x64.exe - Ruby for Windows (x64);
* Run "gem install sass"
* Inside the project's folder run "npm install";
* Run "grunt" for development build (see developer tools for more info);
* Run "grunt build" for production build (minified, linted, browser compatibility check, consoles off, etc);
* Run through your own HTTP server;

### Structure ###

* Running either the "grunt" or "grunt build" command will generate files inside the "dest" folder (configurable under "grunt/constants.grunt.config.js");
* Running the "grunt build" command will also generate a timestamped archive (configurable also);