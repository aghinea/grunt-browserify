var HelloWorld = require('./helloWorld');

var Application = (function () {
    'use strict';
    return {
        init: function () {
            alert(HelloWorld.exampleFunction('John'));
        }
    };
})();

Application.init();