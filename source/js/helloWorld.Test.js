var expect = require("chai").expect;
var describe = require('mocha').describe;
var it = require('mocha').it;

var HelloWorld = require("./helloWorld");


describe("HelloWorld Suite", function () {
    "use strict";

    describe("HelloWorld.exampleFunction", function () {

        it("Says hello", function () {

            var name = 'Minerva';
            var expected = 'Hello World, Minerva';

            expect(HelloWorld.exampleFunction(name)).to.equal(expected);
        });

    });
});