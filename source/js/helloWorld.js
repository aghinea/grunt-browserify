var HelloWorld = {
    exampleFunction: function (name) {

        'use strict';

        /**
         * @param name: String
         * @returns String
         */
        
        return ['Hello World', name].join(', ');
    }
};

module.exports = HelloWorld;