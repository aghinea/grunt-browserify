var constants = require('./grunt/constants.grunt.config');
var browserifyConfig = require('./grunt/browserify.grunt.config');
var copyConfig = require('./grunt/copy.grunt.config');
var concatConfig = require('./grunt/concat.grunt.config');
var sassConfig = require('./grunt/sass.grunt.config');
var postcssConfig = require('./grunt/postcss.grunt.config');
var jshintConfig = require('./grunt/jshint.grunt.config');
var watchConfig = require('./grunt/watch.grunt.config');
var cssminConfig = require('./grunt/cssmin.grunt.config');
var uglifyConfig = require('./grunt/uglify.grunt.config');
var notifyConfig = require('./grunt/notify.grunt.config');
var execConfig = require('./grunt/exec.grunt.config');
var mochaTest = require('./grunt/mochaTest.grunt.config');
var compress = require('./grunt/compress.grunt.config');
var clean = require('./grunt/clean.grunt.config');
var rename = require('./grunt/rename.grunt.config');
var prettify = require('./grunt/prettify.grunt.config');
var stringReplace = require('./grunt/stringReplace.grunt.config');

module.exports = function (grunt) {

    require('time-grunt')(grunt);

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-rename');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-rename');
    grunt.loadNpmTasks('grunt-string-replace');

    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-prettify');
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-mocha-test');

    grunt.registerTask('default', ['clean', 'copy', 'concat', 'sass', 'cssmin', 'mochaTest', 'jshint', 'browserify:dev', 'string-replace', 'rename', 'notify:dev']);
    grunt.registerTask('build', ['clean', 'copy', 'concat', 'sass', 'cssmin', 'mochaTest', 'jshint', 'browserify:production', 'uglify:production', 'postcss', 'string-replace', 'rename', 'prettify', 'compress', 'notify:production']);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: copyConfig,
        concat: concatConfig,
        sass: sassConfig,
        postcss: postcssConfig,
        jshint: jshintConfig,
        uglify: uglifyConfig,
        browserify: browserifyConfig,
        cssmin: cssminConfig,
        watch: watchConfig,
        notify: notifyConfig,
        exec: execConfig,
        mochaTest: mochaTest,
        clean: clean,
        compress: compress,
        rename: rename,
        prettify: prettify,
        'string-replace': stringReplace
    });
};