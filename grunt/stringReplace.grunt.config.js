var constants = require("./constants.grunt.config");

module.exports = {
    production: {
        files: {
            'dest/': [constants.dest + '**/*.js', constants.dest + '**/*.json', constants.dest + '**/*.html', constants.dest + '**/*.css'],
        },
        options: {
            replacements: [
                /**
                 * @description  Looks for {releaseVersion}, {releasePackageName}, 
                 * {releaseLanguage}, {releaseIcons}, {releaseBody}, {releaseJS}, {releaseCSS} in the entire de "constants.dest" folder
                 */
                {
                    pattern: /<!--{releaseVersion}-->/gm,
                    replacement: constants.release.version
                },
                {
                    pattern: /<!--{releasePackageName}-->/gm,
                    replacement: constants.release.packageName
                },
                {
                    pattern: /<!--{releaseLanguage}-->/gm,
                    replacement: 'en'
                },
                {
                    pattern: /<!--{releaseName}-->/gm,
                    replacement: constants.release.brand
                },
                {
                    pattern: /<!--{releaseJS}-->/gm,
                    replacement: constants.staticJS + constants.release.pattern.replace('{fileName}', constants.release.appFileName).replace('{tag}', constants.release.tag).replace('{extension}', 'min.js')
                },
                {
                    pattern: /<!--{releaseCSS}-->/gm,
                    replacement: constants.staticCSS + constants.release.pattern.replace('{fileName}', constants.release.appFileName).replace('{tag}', constants.release.tag).replace('{extension}', 'min.css')
                },
                {
                    pattern: /<!--{vendorsCSS}-->/gm,
                    replacement: constants.staticCSS + constants.release.pattern.replace('{fileName}', constants.release.vendorFileName).replace('{tag}', constants.release.tag).replace('{extension}', 'min.css')
                }
            ]
        }
    }
};