var constants = require("./constants.grunt.config");
module.exports = {
    options: {
        banner: constants.copyrightBanner,
        compress: {
            drop_console: true
        }
    },
    production: {
        files: {
            [(constants.dest + constants.staticJS) + constants.release.appFileName + '.min.js']: [constants.sourceTemp + constants.release.appFileName + '.bundle.js']
        }
    }
};