﻿var constants = require("./constants.grunt.config");
module.exports = {
    options: {
        indent: 2,
        indent_char: ' ',
        indent_scripts: '',
        wrap_line_length: 0,
        brace_style: 'collapse',
        preserve_newlines: true,
        max_preserve_newlines: 1,
        unformatted: [
          "a",
          "code",
          "pre"
        ]
    },
    production: {
        expand: true,
        cwd: constants.dest,
        ext: '.html',
        src: ['*.html'],
        dest: constants.dest
    }
};