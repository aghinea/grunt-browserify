var constants = require("./constants.grunt.config");
module.exports = {
    default: {
        files: [
            { src: [(constants.dest + constants.staticCSS) + 'app.min.css'], dest: (constants.dest + constants.staticCSS) + constants.release.pattern.replace('{fileName}', constants.release.appFileName).replace('{tag}', constants.release.tag).replace('{extension}', 'min.css') },
            { src: [(constants.dest + constants.staticCSS) + 'vendors.min.css'], dest: (constants.dest + constants.staticCSS) + constants.release.pattern.replace('{fileName}', constants.release.vendorFileName).replace('{tag}', constants.release.tag).replace('{extension}', 'min.css') },
            { src: [(constants.dest + constants.staticJS) + 'release.min.js'], dest: (constants.dest + constants.staticJS) + constants.release.pattern.replace('{fileName}', constants.release.appFileName).replace('{tag}', constants.release.tag).replace('{extension}', 'min.js') },
        ]
    }
};