/**
 * @description Generates a release package
 */
var constants = require("./constants.grunt.config");
module.exports = {
    production: {
        options: {
            archive: [constants.release.appFileName, constants.release.version, 'zip'].join('.')
        },
        files: [
            {
                expand: true,
                cwd: constants.dest,
                src: ['**/*'],
                dest: constants.dest
            }
        ]
    }
};