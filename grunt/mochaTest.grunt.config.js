var constants = require("./constants.grunt.config");
module.exports = {
    default: {
        options: {
            reporter: 'spec'
        },
        src: [[constants.source, '**/*Test.js'].join('')]
    }
};