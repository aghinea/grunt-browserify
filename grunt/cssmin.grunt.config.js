var constants = require("./constants.grunt.config");
module.exports = {
    default: {
        files: [
            {
                expand: true,
                cwd: constants.sourceTemp,
                src: ['*.css', '!*.min.css'],
                dest: [constants.dest, constants.staticCSS].join(''),
                ext: '.min.css'
            }
        ]
    }
};