var constants = require("./constants.grunt.config");
module.exports = {
    default: {
        options: {
            style: 'expanded'
        },
        files: [
            {
                expand: true,
                cwd: constants.sourceSCSS,
                src: ['app.scss'],
                dest: constants.sourceTemp,
                ext: ('.{tag}.{extension}').replace('{tag}', constants.release.tag).replace('{extension}', 'css')
            }
        ]
    }
};