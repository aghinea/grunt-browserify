var constants = require("./constants.grunt.config");
module.exports = {
    transferFilesToDest: {
        options: {
            processContentExclude: [constants.dest + 'assets/*.{png,gif,jpg,ico,mp4,webm}']
        },
        files: [
            {
                expand: true,
                cwd: constants.sourceAssets,
                src: '**',
                dest: constants.dest + 'assets/',
            },
            {
                expand: true,
                cwd: constants.sourceStatic,
                src: '**',
                dest: constants.dest + 'static/',
            },
            {
                expand: true,
                cwd: constants.source,
                src: ['index.html'],
                dest: constants.dest
            }
        ]
    }
};