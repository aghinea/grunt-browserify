/**
 * @description Creates the main JavaScript bundle for development and production
 */

var constants = require("./constants.grunt.config");
module.exports = {
    options: {
        banner: constants.copyrightBanner,
        transform: ['browserify-global-shim']
    },
    production: {
        options: {
            browserifyOptions: {
                debug: constants.debug
            }
        },
        src: constants.sourceJS + 'app.js',
        dest: [constants.sourceTemp, constants.release.appFileName, '.bundle.js'].join('')
    },
    dev: {
        options: {
            browserifyOptions: {
                debug: constants.debug
            },
            plugin: [
                //['minifyify', {map: constants.mapFile, output: constants.mapOutput}]
            ]
        },
        src: [constants.sourceJS, 'app.js'].join(''),
        dest: [constants.dest, constants.staticJS, constants.release.appFileName, '.min.js'].join('')
    }
};