/**
 * @description Cleans the "destination" and the "temporary"
 */
var constants = require("./constants.grunt.config");
module.exports = {
    sourceTemp: [constants.sourceTemp],
    des: [constants.dest]
};