module.exports = {
    release: {
        brand: '<%= pkg.brand.name %>',
        brandPrimaryColor: '<%= pkg.brand.color.primary %>',
        brandAccentColor: '<%= pkg.brand.color.accent %>',
        tag: '<%= pkg.version %>.<%= grunt.template.today("yyyymmddhh")%>',
        pattern: '{fileName}.{tag}.{extension}',
        appFileName: 'release',
        vendorFileName: 'vendors',
        version: '<%= pkg.version %>',
        packageName: '<%= pkg.name %>',
    },
    nodeModules: 'node_modules/',
    source: 'source/',
    sourceJS: 'source/js/',
    sourceSCSS: 'source/scss/',
    sourceTemp: 'source_temp/',
    sourceAssets: 'source/assets/',
    sourceStatic: 'source/static',
    jsxComponents: 'components/',
    dest: 'dest/', //@where the build files are generated
    destAssets: 'assets/', //@where assets files are served from
    staticCSS: 'static/css/', //@where CSS files are copied to
    staticJS: 'static/js/',  //@where JS files are copied to
    mapFile: '../source_temp/release.min.map',
    mapOutput: 'source_temp/release.min.map',
    debug: true,
    copyrightBanner: '/*Copyright <%= pkg.name %>, <%= grunt.template.today("yyyy-mm-dd") %> */\n',
    notify: {
        options: {
            enabled: true,
            max_jshint_notifications: 5, // maximum number of notifications from jshint output
            success: true, // whether successful grunt executions should be notified automatically
            duration: 10 // the duration of notification in seconds, for `notify-send only
        }
    }
};